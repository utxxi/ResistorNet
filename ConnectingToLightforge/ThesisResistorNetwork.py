#!/usr/bin/env python
# coding: utf-8

# # Combination of all important functions
# This is a summary of the important functions for constructing a network and solving it for the currents.   
# First off there is the Construct function which takes the paths to the input data and builds a corresponding Networkx-Graph from it.  
# Then there ist the Solve class which takes a network as input and can then construct the matrices and vectors needed to solve for the currents.   
# In the end there are functions to select the currents flowing throuhg one layer an sum them up.


WDir="./"



import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import random
from scipy import sparse
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
from scipy.stats import norm
from scipy.optimize import curve_fit
from scipy.spatial.distance import euclidean
import pandas as pd
from decimal import Decimal


def Gauss(x, mu, sig, A):
    return A  *np.exp(-0.5 * ((x-mu)/sig)**2) * 1 /(np.sqrt(2 * np.pi * sig **2))

def Load(fname, Split=True):
    a=(np.genfromtxt(fname, delimiter =" ")).flatten()    
    if Split==True:
        #Lightforge uses an high but finite value (approx 300) to handle hopping onto already taken sites
        low=a[np.abs(a)<300]
        return low
    else:
        return a

def FitGauss(data):
    bin_heights, bin_borders, _ = plt.hist(data, bins='auto')
    bin_centers = bin_borders[:-1] + np.diff(bin_borders) / 2
    popt, _ = curve_fit(Gauss, bin_centers, bin_heights, p0=[0.01, 0.009, len(data)/5])
    plt.close()
    return popt, bin_borders




def miller_abrahams_rate(J, delta_E, T_eff):
    h_bar = 1.05457173e-34  # Si
    delta_E = (delta_E + np.absolute(delta_E)) / 2.0
    miller_prefactor = np.pi / (h_bar * 2 * T_eff)
    rate = miller_prefactor * J * np.exp(-delta_E / T_eff)
    return rate
    

def miller_j(j0,a0,distances,r0=0.0):
    #Note the square to match lightforge implementation
    R = distances
    j_r0 = np.exp(-4*a0*r0)
    return (j0**2* np.clip(np.exp(-4*a0*R), a_min=0.0, a_max=j_r0))    
    
def Construct(Coo=WDir+"results/material/device_data/coord_0.dat",#
              Typ=WDir+"results/material/device_data/mol_types_0.dat",#
              Ener=WDir+"results/material/device_data/site_energies_0.dat",#
              CoulombHoles=WDir+"rates/1_100000_holes_dE_coulomb.dat",#
              CoulombElectrons=WDir+"rates/1_100000_electrons_dE_coulomb.dat",#
              ChargeDist=WDir+"/results/experiments/particle_densities/all_data_points/site_charge_density_0.npz",#
              InjectionType="h",# for electrode simulation there are otherwise HOMO and LUMO in the same file
              J0=0.001, decay=0.1, T=300):
    #from settings file
    #J = attempt frequency (largest possible rate) =>  "maximum ti"
    k_boltzmann = 1.3806488e-23  # Si   # 0.00008617 in eV
    kbT=k_boltzmann*T #Si
    H_Bar = 1.05457173e-34  # Si
    Qe = 1.602176634*(10**(-19)) # Electron charge
    J0*=Qe #To match lightforge implementation
    
    #Read coordinates from file
    coords = list(map(tuple, np.genfromtxt(Coo, delimiter=" ")))
    
    #Build Graph
    F=nx.Graph()
    F.add_nodes_from(coords)
    #Read Attributes from type dependent part of the file
    types = np.genfromtxt(Typ, delimiter=" ")
    if InjectionType=="h":
        energies = np.genfromtxt(Ener, delimiter=" ")[:,0]
        VZ=1
        #Read occupation probabilites from file
        occ=np.load(ChargeDist)["hole_density"]
    elif InjectionType=="e":
        energies = np.genfromtxt(Ener, delimiter=" ")[:,1]
        VZ=-1
        #Read occupation probabilites from file
        occ=np.load(ChargeDist)["electron_density"]
    else:
        energies = np.genfromtxt(Ener, delimiter=" ")
        VZ=1
        #Read occupation probabilites from file
        occ=np.load(ChargeDist)["hole_density"]
    #Set Attributes
    i=0
    for u in F.nodes():
        F.nodes[u]["pos"]=u
        F.nodes[u]["type"]=types[i]
        F.nodes[u]["energies"]=energies[i]
        F.nodes[u]["occ"]=occ[i]
        i+=1  
    #Construct Edges
    F.add_edges_from(nx.geometric_edges(F, 1.9))
    
    #Calculate Distribution for Coulombenergydifferences
#     ele=Load(CoulombElectrons)
#     hol=Load(CoulombHoles)
#     data=np.concatenate((ele, hol))
#     popt, bin_borders=FitGauss(data)
    
    ## Inizializing Miller Rates ##
    for (u, v) in F.edges():     
        
        ## v is start and u is end and vice versa
        ## VZ Prefactor to favor energetical down/up hill hopping of electrons/holes
        deltaE=(F.nodes[u]["energies"] - F.nodes[v]["energies"]) #+ np.random.default_rng().normal(popt[0], popt[1]) 
        deltaE*= 1.60218e-19 #Si
        deltaE*=-1* VZ 
        J=miller_j(J0,decay,(euclidean(u,v)), 1.0)
        
        rateVU=miller_abrahams_rate(J, deltaE , kbT)*F.nodes[v]["occ"]*(1-F.nodes[u]["occ"])
        rateUV=miller_abrahams_rate(J, -deltaE , kbT)*F.nodes[u]["occ"]*(1-F.nodes[v]["occ"])

        conductance =   (Qe**2 * (rateVU-rateUV) )/deltaE  #effective current due to occupation probability and backhopping
        F.edges[u,v]['weight'] = conductance
        
    return F


class Solve:
    def __init__(self, X):
        self.G=X
        self.N=self.G.order()
        self.nR=self.G.number_of_edges()
        
    def conductances(self):
        #Extract the values of the resistors from the graph and build a nR x nR matrix
        mat=sparse.spdiags(np.asarray(list((nx.get_edge_attributes(self.G, "weight").values()))), 0, self.nR, self.nR)
        return sparse.csc_matrix(mat)
    
    def incidence(self):
        #Builds the incidence matrix from the graph
        mat= np.transpose(nx.incidence_matrix(self.G, oriented=1)) #transpose to match networkx function to theoretical definition used
        
        
        
        return mat
    
    def voltages(self):
        #Get the potential values from the nodes and build a vector
        vec=np.array(list(nx.get_node_attributes(self.G, "energies").values()))
        return vec

    
    def currents(self):
        #Combines the other functions to get the currents trough the resistors
        return -(self.conductances() @ self.incidence()) @ self.voltages()

    

    # Parse out nodes and the current flowing into these nodes
    def node_currents(self, layer=0, Save=True):
        lowXEnd=np.array(self.G.edges())[:,:,0]==np.min(np.array(self.G.nodes())[:,0])+layer # get individual arrays with x values of nodes and check if they are from the right layer in x direction
        lowFilter=np.logical_xor(lowXEnd[:,0],lowXEnd[:,1]) # exclude in plane connections
        relevantCurrents=self.currents()[lowFilter]
        relevantNodes=(np.array(self.G.edges())[lowFilter,:])[lowXEnd[lowFilter]] #Nodes out of the edge List in this plane
        resultingNodeCurrents=np.hstack([relevantNodes[0],np.sum(relevantCurrents[np.all(relevantNodes==relevantNodes[0], axis=1)])])
        for i in relevantNodes:
            newLine=np.hstack([i,np.sum(relevantCurrents[np.all(relevantNodes==i, axis=1)])])
            resultingNodeCurrents=np.vstack([resultingNodeCurrents,newLine])
        UniqueNodeCurrents=np.unique(resultingNodeCurrents, axis=0)
        if Save==True:
            np.save(f'{WDir}NodeCurrents.npy', UniqueNodeCurrents)
        return UniqueNodeCurrents



    # Calculate layer currents through layer perpendicular to x-axis

    def total_layer(self, layer):
    # Note the XOR filter logic might be only applicable to the endcaps
    #       for other layers this will both count incoming and outgoing current
    #       if this funcionality is needed => mabye divide by two?
        lowXEnd=np.array(self.G.edges())[:,:,0]==np.min(np.array(self.G.nodes())[:,0])+layer
        lowFilter=np.logical_xor(lowXEnd[:,0],lowXEnd[:,1])
        total_IN=np.sum(self.currents()[lowFilter])
        return total_IN /2 #either divide by two or decide on incoming/outgoing current in the future
    
    
    def total_in(self):
        lowXEnd=np.array(self.G.edges())[:,:,0]==np.min(np.array(self.G.nodes())[:,0])
        lowFilter=np.logical_xor(lowXEnd[:,0],lowXEnd[:,1])
        total_IN=np.sum(self.currents()[lowFilter])
        return total_IN
    
    def total_out(self):
        highXEnd=np.array(self.G.edges())[:,:,0]==np.max(np.array(self.G.nodes())[:,0])
        highFilter=np.logical_xor(highXEnd[:,0],highXEnd[:,1])
        total_OUT=np.sum(self.currents()[highFilter])
        return total_OUT


def Format(value):
    return f"{Decimal(value):.2E}"


# ##### This is a minimal working example:
#    Gitter=Construct()
#    Setup=Solve(Gitter)
#    total_OUT=Setup.total_out()
#    print("Output in mA/cm^2") #for 20x20 grid
#    print(Format(float(total_OUT)/((20*10**-9)**2) * ((10**-2)**2) *1000))
# #### Mind that I used a 20x20 grid for the unit conversion at the end - this has to be adapted fot other geometries!



# ### Setup for generation of U-I-Plot data
exp=["experiment1"] #relative pathways to lightforge outputs
voltages=range(4)
size=20 #grid side length
for U in voltages:
    worker= np.arange(20)+U*20
    for i in exp:
        result=np.array([])
        for w in worker:
            WDir=f"./"
            Gitter=Construct(Coo=WDir+f"results/material/device_data/coord_{w}.dat",#
                      Typ=WDir+f"results/material/device_data/mol_types_{w}.dat",#
                      Ener=WDir+f"results/material/device_data/site_energies_{w}.dat",#
                      CoulombHoles=WDir+"rates/1_100000_holes_dE_coulomb.dat",#
                      CoulombElectrons=WDir+"rates/1_100000_holes_dE_coulomb.dat",#
                      ChargeDist=WDir+f"/results/experiments/particle_densities/all_data_points/site_charge_density_{w}.npz",#
                      InjectionType="h",# for electrode simulation there are otherwise HOMO and LUMO in the same file
                      J0=0.001, decay=0.1, T=300)
            Setup=Solve(Gitter)
            total_OUT=Setup.total_out()
            total_IN=Setup.total_in()

            cDensity=(float((total_OUT+total_IN)/2)/((size*10**-9)**2)) * ((10**-2)**2) *1000

            result=np.append(result,cDensity)

            np.save(f"{WDir}NodeCurrents_{w}", Setup.node_currents(Save=False))

        with open(WDir+f'results/experiments/current_characteristics/current_density_OP_{U}.dat', 'w') as f:
            print(Format(np.mean(result)), file=f)
            print(Format(np.std(result)), file=f)
        print(f"{i} Stromdichte über alle Worker: {Format(np.mean(result))} +/- {Format(np.std(result))}")



